//
//  GameDetailResponse.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 08/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation

struct GameDetailResponse: Codable {
    let id: Int
    let name: String
    let rating: Double
    let playtime: Int
    let description: String
    let website: String
    let clip: Clip
    let developers: [Developer]
    let platforms: [Platform]
    let stores: [Store]
    
    enum CodingKeys: String, CodingKey {
        case id, name, rating, playtime, description, website
        case clip
        case developers, platforms, stores
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        rating = try values.decodeIfPresent(Double.self, forKey: .rating) ?? 0.0
        playtime = try values.decodeIfPresent(Int.self, forKey: .playtime) ?? 0
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? ""
        website = try values.decodeIfPresent(String.self, forKey: .website) ?? ""
        clip = try values.decodeIfPresent(Clip.self, forKey: .clip) ?? Clip()
        developers = try values.decodeIfPresent([Developer].self, forKey: .developers) ?? []
        platforms = try values.decodeIfPresent([Platform].self, forKey: .platforms) ?? []
        stores = try values.decodeIfPresent([Store].self, forKey: .stores) ?? []
    }
}

struct Store: Codable {
    let url: String
    let store: DetailStore
    
    enum CodingKeys: String, CodingKey {
        case url, store
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decodeIfPresent(String.self, forKey: .url) ?? ""
        store = try values.decodeIfPresent(DetailStore.self, forKey: .store)!
    }
}

struct DetailStore: Codable {
    let name: String
    let imageBackground: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageBackground = "image_background"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        imageBackground = try values.decodeIfPresent(String.self, forKey: .imageBackground) ?? ""
    }
}

struct Clip: Codable {
    let clip: String
    let preview: String
    
    enum CodingKeys: String, CodingKey {
        case clip, preview
    }
    
    init() {
        self.clip = ""
        self.preview = ""
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        clip = try values.decodeIfPresent(String.self, forKey: .clip) ?? ""
        preview = try values.decodeIfPresent(String.self, forKey: .preview) ?? ""
    }
}

struct Developer: Codable {
    let id: Int
    let name: String
    let imageBackground: String
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case imageBackground = "image_background"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        imageBackground = try values.decodeIfPresent(String.self, forKey: .imageBackground) ?? ""
    }
}

struct Platform: Codable {
    let platform: DetailPlatform
    
    enum CodingKeys: String, CodingKey {
        case platform
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        platform = try values.decodeIfPresent(DetailPlatform.self, forKey: .platform) ?? DetailPlatform()
    }
}

struct DetailPlatform: Codable {
    let name: String
    let imageBackground: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageBackground = "image_background"
    }
    
    init() {
        self.name = ""
        self.imageBackground = ""
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        imageBackground = try values.decodeIfPresent(String.self, forKey: .imageBackground) ?? ""
    }
}

