//
//  ProfileModel.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 23/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation

struct ProfileModel {
    var id: Int16?
    var profileImage: Data?
    var name: String?
    var email: String?
}
