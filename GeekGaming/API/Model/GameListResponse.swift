//
//  GameListResponse.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 08/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation
import UIKit

struct GameListResponse: Codable {
    let count: Int
    let next: String
    let previous: String
    let games: [GamesListResult]
    
    enum CodingKeys: String, CodingKey {
        case count
        case next
        case previous
        case games = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decodeIfPresent(Int.self, forKey: .count) ?? 0
        next = try values.decodeIfPresent(String.self, forKey: .next) ?? ""
        previous = try values.decodeIfPresent(String.self, forKey: .previous) ?? ""
        games = try values.decodeIfPresent([GamesListResult].self, forKey: .games) ?? []
    }
}

struct GamesListResult: Codable {
    let id: Int
    let name: String
    let released: String
    let backgroundImage: String
    let rating: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case released
        case backgroundImage = "background_image"
        case rating
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        released = try values.decodeIfPresent(String.self, forKey: .released) ?? ""
        backgroundImage = try values.decodeIfPresent(String.self, forKey: .backgroundImage) ?? ""
        rating = try values.decodeIfPresent(Double.self, forKey: .rating) ?? 0.0
    }
}
