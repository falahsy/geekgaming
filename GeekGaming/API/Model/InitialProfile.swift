//
//  InitialProfile.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 23/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation
import UIKit

var initialProfileFalah: ProfileModel = ProfileModel(id: 1, profileImage: imageToData("profile-falah"), name: "Syamsul Falah", email: "syamsulfalah22@gmail.com")

func imageToData(_ title: String) -> Data?{
    guard let img = UIImage(named: title) else { return nil }
    return img.jpegData(compressionQuality: 1)
}
