//
//  GlobalData.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 09/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation

class GlobalData {
    static let shared = GlobalData()
    
    private init() {}
    
    func getBaseUrl() -> String {
        return "https://api.rawg.io/api/"
    }
}
