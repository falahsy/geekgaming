//
//  ProfileProvider.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 23/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ProfileProvider {
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Submission")
        container.loadPersistentStores { (storeDescription, error) in
            guard error == nil else {
                fatalError("Unresolved error \(String(describing: error))")
            }
        }
        container.viewContext.automaticallyMergesChangesFromParent = false
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        container.viewContext.shouldDeleteInaccessibleFaults = true
        container.viewContext.undoManager = nil
        
        return container
    }()
    
    private func newTaskContext() -> NSManagedObjectContext {
        let taskContext = persistentContainer.newBackgroundContext()
        
        taskContext.undoManager = nil
        taskContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        return taskContext
    }
    
    func createProfile(imageProfile: Data, name: String, email: String, completion: @escaping ()->()) {
        let taskContext = newTaskContext()
        taskContext.performAndWait {
            if let entity = NSEntityDescription.entity(forEntityName: "Profile", in: taskContext) {
                let profile = NSManagedObject(entity: entity, insertInto: taskContext)
                
                profile.setValue(1, forKey: "id")
                profile.setValue(name, forKey: "name")
                profile.setValue(email, forKey: "email")
                profile.setValue(imageProfile, forKey: "profileImage")
                
                do {
                    try taskContext.save()
                    completion()
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
        }
    }
    
    func getProfile(id: Int, completion: @escaping (_ profile: ProfileModel)->() ) {
        let taskContext = newTaskContext()
        taskContext.perform {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Profile")
            fetchRequest.fetchLimit = 1
            fetchRequest.predicate = NSPredicate(format: "id == \(id)")
            
            do {
                if let result = try taskContext.fetch(fetchRequest).first {
                    let profile = ProfileModel(id: result.value(forKey: "id") as? Int16,
                                               profileImage: result.value(forKey: "profileImage") as? Data,
                                               name: result.value(forKey: "name") as? String,
                                               email: result.value(forKey: "email") as? String)
                    completion(profile)
                }
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func updateProfile(id: Int, imageProfile: Data, name: String, email: String, completion: @escaping ()->() ) {
        let taskContext = newTaskContext()
        taskContext.perform {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Profile")
            fetchRequest.fetchLimit = 1
            fetchRequest.predicate = NSPredicate(format: "id == \(id)")
            if let result = try? taskContext.fetch(fetchRequest), let profile = result.first as? Profile {
                profile.setValue(imageProfile, forKey: "profileImage")
                profile.setValue(name, forKey: "name")
                profile.setValue(email, forKey: "email")
                
                do {
                    try taskContext.save()
                    completion()
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
        }
    }
    
    func initialProfile(completion: @escaping() -> ()) {
        let profile = initialProfileFalah
        if let image = profile.profileImage, let name = profile.name, let email = profile.email {
            self.createProfile(imageProfile: image, name: name, email: email) {
                completion()
            }
        }
    }
}
