//
//  GameListInteractor.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 08/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import Foundation

class GameInteractor {
    
    func getListGame(endPoint: String, search: String, ordering: String, page: Int, completion: @escaping (_ data: GameListResponse?) -> Void ) {
        let url = GlobalData.shared.getBaseUrl()+endPoint
        var components = URLComponents(string: url)!
        
        components.queryItems = [
            URLQueryItem(name: "search", value: search),
            URLQueryItem(name: "page", value: String(page)),
            URLQueryItem(name: "ordering", value: ordering)
        ]
        
        let request = URLRequest(url: components.url!)
        
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration)
        
        if let response = configuration.urlCache?.cachedResponse(for: request) {
            do {
                let result = try JSONDecoder().decode(GameListResponse.self, from: response.data)
                completion(result)
            } catch {
                print("Failed to decode JSON")
            }
        } else {
            let task = session.dataTask(with: request) { data, response, error in
                guard let response = response as? HTTPURLResponse, let data = data else { return }
                
                if response.statusCode == 200 {
                    do {
                        let result = try JSONDecoder().decode(GameListResponse.self, from: data)
                        completion(result)
                    } catch {
                        print("Failed to decode JSON")
                    }
                } else {
                    print("Something Error Happened")
                }
            }
            task.resume()
        }
    }
    
    func getDetailGame(endPoint: String, id: String, completion: @escaping (_ data: GameDetailResponse?) -> Void) {
        let url = GlobalData.shared.getBaseUrl()+endPoint+id
        let components = URLComponents(string: url)!
        let request = URLRequest(url: components.url!)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let response = response as? HTTPURLResponse, let data = data else { return }
            
            if response.statusCode == 200 {
                do {
                    let result = try JSONDecoder().decode(GameDetailResponse.self, from: data)
                    completion(result)
                } catch {
                    print("Failed to decode JSON")
                }
            } else {
                print("Something Error Happened")
            }
        }
        task.resume()
    }
    
}
