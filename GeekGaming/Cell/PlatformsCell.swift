//
//  PlatformsCell.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 12/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class PlatformsCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpView()
    }
    
    func setUpView() {
        mainView.layer.cornerRadius = 10
        
        mainView.layer.borderWidth = 0.5
        mainView.layer.borderColor = UIColor.gray.cgColor
    }
}
