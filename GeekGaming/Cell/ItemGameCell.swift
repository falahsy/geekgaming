//
//  ItemGameCell.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 15/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class ItemGameCell: UITableViewCell {

    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var titleGame: UILabel!
    @IBOutlet weak var ratingGame: UILabel!
    @IBOutlet weak var yearReleaseGame: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var yearReleaseImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        gameImage.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
