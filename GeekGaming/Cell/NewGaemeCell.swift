//
//  NewGaemeCell.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 11/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class GameCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageGame: UIImageView!
    @IBOutlet weak var titleGame: UILabel!
    @IBOutlet weak var ratingGame: UILabel!
    @IBOutlet weak var timeReleasedGame: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var buttomView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        mainView.layer.cornerRadius = 10
    }
}
