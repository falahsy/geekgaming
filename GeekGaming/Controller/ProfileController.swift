//
//  ProfileController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 12/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class ProfileController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    private lazy var profileProvider: ProfileProvider = { return ProfileProvider() }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Profile"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        
        loadProfile()
    }
    
    func loadProfile() {
        profileProvider.getProfile(id: 1) { (profile) in
            DispatchQueue.main.async {
                self.nameLabel.text = profile.name
                self.emailLabel.text = profile.email
                if let image = profile.profileImage {
                    self.profileImageView.image = UIImage(data: image)
                    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                    self.profileImageView.clipsToBounds = true
                }
            }
        }
    }
}
