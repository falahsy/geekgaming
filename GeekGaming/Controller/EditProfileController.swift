//
//  EditProfileController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 23/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class EditProfileController: UIViewController {

    @IBOutlet weak var addImageProfile: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var editImage: UIImageView!
    
    private lazy var profileProvider: ProfileProvider = {
        return ProfileProvider()
    }()
    
    var nameIsFilled = false
    var emailIsFilled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupContent()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .never
        
        self.tabBarController?.tabBar.isHidden = true
        loadProfile()
    }
    
    func setupContent() {
        addImageProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeProfileImage(_ :))))
    }
    
    func setupView() {
        saveButton.layer.cornerRadius = 10
        addImageProfile.layer.cornerRadius = addImageProfile.frame.height/2
        
        nameTextField.placeholder = "Input your name"
        emailTextField.placeholder = "Input your email"
    }
    
    @objc func changeProfileImage(_ sender: UITapGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        guard let name = nameTextField.text, name != "" else {
            alert("Field name is empty")
            return
        }
        
        guard let email = emailTextField.text, email != "" else {
            alert("Field email is empty")
            return
        }
        
        if let image = addImageProfile.image, let data = image.pngData() as NSData? {
            profileProvider.updateProfile(id: 1, imageProfile: data as Data, name: name, email: email) {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Successful", message: "Profile updated.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func loadProfile() {
        profileProvider.getProfile(id: 1) { (profile) in
            DispatchQueue.main.async {
                self.nameTextField.text = profile.name
                self.emailTextField.text = profile.email
                if let image = profile.profileImage {
                    self.addImageProfile.image = UIImage(data: image)
                    self.addImageProfile.layer.cornerRadius = self.addImageProfile.frame.height/2
                    self.addImageProfile.clipsToBounds = true
                }
            }
        }
    }
    
    func alert(_ message: String ){
        let allertController = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        allertController.addAction(alertAction)
        self.present(allertController, animated: true, completion: nil)
    }
}

extension EditProfileController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage: UIImage
        
        if let possibleImage = info[.editedImage] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info[.originalImage] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }
        addImageProfile.image = newImage
        dismiss(animated: true)
    }
}
