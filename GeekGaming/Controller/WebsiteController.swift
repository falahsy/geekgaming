//
//  WebsiteController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 12/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit
import WebKit

class WebsiteController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var strUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.isHidden = false
        indicator.startAnimating()
        
        if strUrl != "" {
            loadWebsite(strUrl: strUrl)
        }
    }
    
    func loadWebsite(strUrl: String) {
        let url = URL(string: strUrl)!
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
    }
}

extension WebsiteController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicator.isHidden = true
        indicator.stopAnimating()
    }
}
