//
//  OrderedController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 12/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

protocol OrderedControllerDelegate {
    func applyOrdered(applyBy: String)
}

class OrderedController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var underlineView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var applyButton: UIButton!
    
    let orderingOption = [
        "Name",
        "Released",
        "Added",
        "Created",
        "Rating"
    ]
    
    var orderingChoosed = ""
    var delegate: OrderedControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpContent()
    }
    
    func setUpView() {
        containerView.layer.cornerRadius = 10
        underlineView.layer.cornerRadius = 3
        applyButton.layer.cornerRadius = 10
    }
    
    func setUpContent() {
        orderingChoosed = orderingOption[0].lowercased()
    }
    
    @IBAction func applyTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.applyOrdered(applyBy: orderingChoosed)
        }
        dismiss(animated: true, completion: nil)
    }
}

extension OrderedController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return orderingOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return orderingOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        orderingChoosed = orderingOption[row].lowercased()
    }
}
