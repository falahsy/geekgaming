//
//  DetailGameController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 11/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit

class DetailGameController: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var indicatorImage: UIActivityIndicatorView!
    
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var playTimeImage: UIImageView!
    @IBOutlet weak var platformTitle: UILabel!
    @IBOutlet weak var storeTitle: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var playTimeLabel: UILabel!
    
    @IBOutlet weak var developerView: UIView!
    @IBOutlet weak var logoImageDeveloper: UIImageView!
    @IBOutlet weak var nameDveloper: UILabel!
    @IBOutlet weak var indocatorLogoDeveloper: UIActivityIndicatorView!
    
    @IBOutlet weak var descriptionGamesLabel: UILabel!
    
    @IBOutlet weak var platformCollectionView: UICollectionView!
    @IBOutlet weak var storesCollectionView: UICollectionView!
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var indicatorViewLoading: UIActivityIndicatorView!
    
    let idPlatformCell = "idPlatformCell"
    let toWebsiteAbout = "toWebsite"
    
    var id: Int = 0
    var nameGame: String = ""
    
    let endPointDetailGame = "games/"
    let interactor = GameInteractor()
    var detailGame: GameDetailResponse?
    
    var listPlatform: [Platform] = []
    var listStores: [Store] = []
    
    var urlToAbout = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpContent()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    func setUpView() {
        self.title = nameGame
        self.logoImageDeveloper.layer.cornerRadius = logoImageDeveloper.frame.height/2
        self.contentView.isHidden = true
        self.indicatorView.isHidden = false
        self.ratingImage.isHidden = true
        self.playTimeImage.isHidden = true
        self.platformTitle.isHidden = true
        self.storeTitle.isHidden = true
    }
    
    func setUpContent() {
        platformCollectionView.register(UINib(nibName: "PlatformsCell", bundle: .main), forCellWithReuseIdentifier: idPlatformCell)
        storesCollectionView.register(UINib(nibName: "PlatformsCell", bundle: .main), forCellWithReuseIdentifier: idPlatformCell)
        getDetailGame()
        
        developerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toPageAboutWebsite(_ :))))
    }
    
    @objc func toPageAboutWebsite(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: toWebsiteAbout, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == toWebsiteAbout {
            if let destVc = segue.destination as? WebsiteController {
                destVc.title = "About Games"
                if urlToAbout != "" {
                    destVc.strUrl = urlToAbout
                }
            }
        }
    }
    
    func getDetailGame() {
        indicatorViewLoading.isHidden = false
        indicatorViewLoading.startAnimating()
        
        interactor.getDetailGame(endPoint: endPointDetailGame, id: String(id)) { (data) in
            guard let data = data else {return}
            
            self.listPlatform = data.platforms
            self.listStores = data.stores
            self.urlToAbout = data.website
            
            DispatchQueue.main.async {
                self.contentView.isHidden = false
                self.indicatorView.isHidden = true
                
                self.indicatorViewLoading.isHidden = true
                self.indicatorViewLoading.stopAnimating()
                
                if data.clip.preview != "" {
                    self.loadImage(url: URL(string: data.clip.preview)!, imageView: self.imageView, indicator: self.indicatorImage)
                } else {
                    self.imageView.image = UIImage(named: "clip-default")
                }
                self.ratingLabel.text = String(data.rating)
                self.playTimeLabel.text = String(data.playtime)+" Hours"
                
                if !data.developers.isEmpty {
                    self.loadImage(url: URL(string: data.developers.first!.imageBackground)!, imageView: self.logoImageDeveloper, indicator: self.indocatorLogoDeveloper)
                    self.nameDveloper.text = data.developers.first?.name
                } else {
                    self.logoImageDeveloper.image = UIImage(named: "clip-default")
                    self.nameDveloper.text = "Developer"
                }
                
                if self.listStores.isEmpty {
                    self.storeTitle.isHidden = true
                } else {
                    self.storeTitle.isHidden = false
                }
                
                if self.listPlatform.isEmpty {
                    self.platformTitle.isHidden = true
                } else {
                    self.platformTitle.isHidden = false
                }
                
                self.ratingImage.isHidden = false
                self.playTimeImage.isHidden = false
                self.descriptionGamesLabel.text = data.description.htmlToString
                
                self.platformCollectionView.reloadData()
                self.storesCollectionView.reloadData()
            }
        }
    }
    
    func loadImage(url: URL, imageView: UIImageView, indicator: UIActivityIndicatorView) {
        indicator.isHidden = false
        indicator.startAnimating()
        DispatchQueue.global(qos: .background).async {
            let dataImage = try? Data(contentsOf: url)
            guard let dataImageResult = dataImage else { return }
            
            let image = UIImage(data: dataImageResult)
            DispatchQueue.main.async {
                imageView.image = image
                indicator.stopAnimating()
                indicator.isHidden = true
            }
        }
    }
}

extension DetailGameController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var row = 0
        if collectionView == self.platformCollectionView {
            row = listPlatform.count
        } else if collectionView == self.storesCollectionView {
            row = listStores.count
        }
        return row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idPlatformCell, for: indexPath) as! PlatformsCell
        
        if collectionView == self.platformCollectionView {
            let data = listPlatform[indexPath.row]
            
            cell.nameLabel.text = data.platform.name
            let url = URL(string: data.platform.imageBackground)!
            self.loadImage(url: url, imageView: cell.image, indicator: cell.indicator)
        } else if collectionView == self.storesCollectionView {
            let data = listStores[indexPath.row]
            
            cell.nameLabel.text = data.store.name
            let url = URL(string: data.store.imageBackground)!
            self.loadImage(url: url, imageView: cell.image, indicator: cell.indicator)
        }
        
        return cell
    }
}

extension DetailGameController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
}
