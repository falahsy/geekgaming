//
//  GameListController.swift
//  GeekGaming
//
//  Created by Syamsul Falah on 09/07/20.
//  Copyright © 2020 Falah. All rights reserved.
//

import UIKit
import Kingfisher

class GameListController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let idTableCell = "idItemGameCell"
    
    let minimumLineSpacing: CGFloat = 30
    let minimumInterIntemSpacing: CGFloat = 0
    let cellPerRows = 2
    
    let interactor = GameInteractor()
    var gameListResponse: GameListResponse?
    var gameList: [GamesListResult]?
    
    var searchController: UISearchController!
    
    let endPointListGame = "games"
    var page: Int = 1
    var idGame = 0
    var nameGame = ""
    
    let sortedButton = UIButton(type: .custom)
    let spinner = UIActivityIndicatorView(style: .medium)
    
    var orderingBy = ""
    var searchQuery = ""
    
    var ordered = false {
        didSet {
            if !ordered {
                sortedButton.setImage(UIImage(named: "sort"), for: .normal)
            } else {
                sortedButton.setImage(UIImage(named: "sort-checked"), for: .normal)
            }
        }
    }
    
    var isLoading = true {
        didSet {
            if isLoading {
                spinner.isHidden = false
                spinner.startAnimating()
                tableView.backgroundView = spinner
            } else {
                spinner.isHidden = true
                spinner.stopAnimating()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ItemGameCell", bundle: .main), forCellReuseIdentifier: idTableCell)
        
        setUpView()
        setUpContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func setUpView() {
        self.title = "Games"
        self.tableView.separatorStyle = .none
        navigationController?.navigationBar.prefersLargeTitles = true
        
        ordered = false
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        
        searchController.searchBar.becomeFirstResponder()
        self.navigationItem.searchController = searchController
        
        sortedButton.addTarget(self, action: #selector(orderedTapped), for: .touchUpInside)
        sortedButton.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        
        sortedButton.widthAnchor.constraint(equalToConstant: 22).isActive = true
        sortedButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        let barButton = UIBarButtonItem(customView: sortedButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func setUpContent() {
        self.isLoading = true
        interactor.getListGame(endPoint: endPointListGame, search: searchQuery, ordering: orderingBy, page: page) { [weak self] data in
            guard let games = data?.games else { return }
            self?.gameListResponse = data
            self?.gameList = games
            
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.tableView.reloadData()
            }
        }
    }
    
    @objc func orderedTapped() {
        performSegue(withIdentifier: "toOrdering", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.hidesBottomBarWhenPushed = true
        
        if segue.identifier == "toDetail" {
            if let destVc = segue.destination as? DetailGameController {
                destVc.id = self.idGame
                destVc.nameGame = self.nameGame
            }
        } else if segue.identifier == "toOrdering" {
            if let destVC = segue.destination as? OrderedController {
                destVC.delegate = self
            }
        }
    }
}

extension GameListController: OrderedControllerDelegate {
    func applyOrdered(applyBy: String) {
        ordered = true
        orderingBy = applyBy
        
        gameList?.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        self.isLoading = true
        interactor.getListGame(endPoint: endPointListGame, search: searchQuery, ordering: orderingBy, page: page) { [weak self] (data) in
            guard let games = data?.games else { return }
            self?.gameListResponse = data
            self?.gameList = games
            
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.tableView.reloadData()
            }
        }
    }
}

extension GameListController: UISearchControllerDelegate, UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        gameList?.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        page = 1
        searchQuery = searchBar.text ?? ""
        
        self.isLoading = true
        interactor.getListGame(endPoint: endPointListGame, search: searchQuery, ordering: orderingBy, page: page) { [weak self] (data) in
            guard let games = data?.games else { return }
            self?.gameList = games
            
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.tableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            gameList?.removeAll()
            page = 1
            searchQuery = ""
            orderingBy = ""
            ordered = false
            tableView.reloadData()
            setUpContent()
        }
    }
}

extension GameListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRow = gameList?.count else {
            return 0
        }
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idTableCell, for: indexPath) as! ItemGameCell
        
        guard let data = gameList?[indexPath.row] else { return cell }
        cell.selectionStyle = .none
        
        let splitDate = data.released.split(separator: "-").map(String.init)
        if !splitDate.isEmpty {
            cell.yearReleaseGame.text = splitDate[0]
        }
        
        let url = URL(string: data.backgroundImage)
        cell.gameImage.kf.indicatorType = .activity
        cell.gameImage.kf.setImage(with: url, placeholder: UIImage(named: "no-image"))
        
        cell.titleGame.text = data.name
        cell.ratingGame.text = String(data.rating)
        cell.ratingImage.image = UIImage(named: "star")
        cell.yearReleaseImage.image = UIImage(named: "timer")

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let game = gameList?[indexPath.row] else {return}
        self.idGame = game.id
        self.nameGame = game.name
        performSegue(withIdentifier: "toDetail", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let response = gameListResponse else { return }
        guard let element = gameList else { return }
        
        let lastElement = element.count - 1
        if indexPath.row == lastElement {
            if response.next != "" {
                self.page += 1

                let spinnerFooter = UIActivityIndicatorView(style: .medium)
                spinnerFooter.startAnimating()
                spinnerFooter.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

                self.tableView.tableFooterView = spinnerFooter
                self.tableView.tableFooterView?.isHidden = false
                
                self.interactor.getListGame(endPoint: endPointListGame, search: searchQuery, ordering: orderingBy, page: page) { [weak self] data in
                    guard let games = data?.games else { return }
                    self?.gameListResponse = data
                    self?.gameList?.append(contentsOf: games)
                    
                    DispatchQueue.main.async {
                        self?.spinner.stopAnimating()
                        self?.tableView.tableFooterView?.isHidden = true
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
}
